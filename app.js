
const express = require("express");
const bodyParser = require('body-parser');
const app = express();
require("dotenv").config();
const mongoose = require("mongoose");
const morgan = require("morgan");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const path = require('path');
const helmet = require('helmet');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Import Router
const authRouter = require("./routes/auth");
const categoryRouter = require("./routes/categories");
const productRouter = require("./routes/products");
const brainTreeRouter = require("./routes/braintree");
const orderRouter = require("./routes/orders");
const usersRouter = require("./routes/users");
const customizeRouter = require("./routes/customize");

// Import Auth middleware for check user login or not~
const { loginCheck } = require("./middleware/auth");

// Database Connection

  mongoose.connect("mongodb+srv://admin:admin131@zuitt.cfuxf.mongodb.net/MCE_Shop?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  });

let db = mongoose.connection;

	db.on("error", console.error.bind(console, "Connection Error"))
	db.once('open', () => console.log('Now connected to MongoDB Atlas'))


// Middleware
app.use(morgan("dev"));
app.use(cookieParser());
app.use(cors());
app.use(express.static("public"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());


// Routes
app.use("/api", authRouter);
app.use("/api/user", usersRouter);
app.use("/api/category", categoryRouter);
app.use("/api/product", productRouter);
app.use("/api", brainTreeRouter);
app.use("/api/order", orderRouter);
app.use("/api/customize", customizeRouter);

if (process.env.NODE_ENV === 'production') {
	app.use(express.static('public'));
}


// Run Server
const server = app.listen(process.env.PORT || 8000, () => {
  const port = server.address().port;
  console.log(`Express is working on port ${port}`);
});
